1. CONFIGURAÇÃO BANCO DE DADOS
------------------------------

  1.1 - Importando o DUMP da aplicação:

     - Abra a IDE usada para o MYSQL, crie o banco 'teste', e importe o DUMP 
     localizado em teste-mkt\assets\Database\teste.sql

  1.2 - Criando acesso para usuário da Aplicação:

     - Crie o usuário 'teste' com a senha '123456' e libere as permissões
     necessárias para acessar o banco de dados 'teste', caso retorne ERRO 404
     verifique o item 1.3.

  1.3 - Alterando URL da Aplicação dentro do Banco de Dados:

     - Abra a tabela 'wp_options' e altera os campos 'siteurl' e 'home' para o local
     correto no ambiente a ser executado.

 2. CONFIGURAÇÃO DO PAINEL WORDPRESS
------------------------------------

  2.1 - Acessando o painel:

    - O usuário admin da aplicação é 'Wallace' e a senha é '123456'.


 3. URL Padrão da Aplicação
------------------------------------
    - http://localhost/wordpress/teste-mkt