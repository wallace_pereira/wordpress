<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title><?php bloginfo() ?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="Wallace">
    <meta name="application-name" content="<?php echo bloginfo('name'); ?>" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/reset.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/libs/slick.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main.css">
</head>
<body>
    <div id="main">
        <?php get_header(); ?>
        <div class="centralizer">
            <div class="content">
                <aside class="banner3">
                    <a href="#">
                        <img src="<?php bloginfo('template_directory'); ?>/assets/images/banner3.jpg" alt="">
                        <p>Preencha a proposta de adesão</p>
                    </a>
                </aside>
                <main>
                    <ul>
                        <?php include(TEMPLATEPATH . '/posts.php'); ?>
                    </ul>
                </main>
            </div>
        </div>
        <section>
            <div class="map">
                <div class="container">
                  <iframe id="map" name="map" src="<?php bloginfo('template_directory'); ?>/map.html"></iframe>
                    <div class="search">
                        <button id="location">
                            Achar minha localização automaticamente
                        </button>
                        <p>ou</p>
                        <form class="send">
                            <label for="cep">Digite o CEP de onde você está</label>
                            <fieldset>
                                <input id="cep-text" type="text" pattern="\d{5}-?\d{3}" placeholder="CEP" id="cep" required>
                                <input type="button" value="Procurar" id="cep-button">
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <section>
                <div class="centralizer">
                    <div class="follow container">
                        <div class="newsletter half">
                            <form action="" class="sender">
                                <label for="newsletter">Assine a newsletter do logo</label>
                                <fieldset>
                                    <input type="email" placeholder="Seu e-mail" id="newsletter">
                                    <input type="submit" id="btn-newsletter" value="Enviar">
                                </fieldset>
                            </form>
                        </div>
                        <div class="social-media half">
                            <p>Siga lojas logo nas redes sociais</p>
                            <ul>
                                <li>
                                    <a href="#" target="_blank" class="facebook"></a>
                                </li>
                                <li>
                                    <a href="#" target="_blank" class="youtube"></a>
                                </li>
                                <li>
                                    <a href="#" target="_blank" class="instagram"></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        <?php get_footer(); ?>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/libs/slick.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/assets/js/banner.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/libs/geoposition.js"></script>
</body>
</html>