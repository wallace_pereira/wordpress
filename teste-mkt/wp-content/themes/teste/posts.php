<?php

    if (have_posts()) : while (have_posts()) : the_post();
        $thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ),'featured');

        echo '<li>';
        echo '<a href="#" style="background-image: url(' . $thumbnail[0] . ');">';

        $tags = null;
        $posttags = get_the_tags();

        if ($posttags) {
            foreach($posttags as $tag) {
                $tags .= $tag->name . ' ';
            }
        }

        echo '<div class="wrap ' . $tags . '">';
        echo '<div class="info">';

        $categories = get_the_category();
        $name = $categories[0]->name;

        if($name != "Sem categoria")
            echo '<h3>' . $name . '</h3>';

        echo '<h2>' . get_the_title() . '</h2>';
        echo '<p>' . the_content() . '</p>';
        echo '<button type="button" id="btn-post">Saiba Mais</button>';

        echo '</div>';
        echo '</div>';
        echo '</a>';
        echo '</li>';

    endwhile; endif;
?>