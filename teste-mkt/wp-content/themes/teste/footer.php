<footer>
    <div class="container">
        <div class="links">
            <ul>
                <li>
                    <h4>Lojas Logo</h4>
                    <ul>
                        <li><a href="#">Campanhas</a></li>
                        <li><a href="#">Casa</a></li>
                        <li><a href="#">Feminino</a></li>
                        <li><a href="#">Kids</a></li>
                        <li><a href="#">Masculino</a></li>
                        <li><a href="#">Promoções</a></li>
                    </ul>
                </li>
                <li>
                    <h4>Lista de Atalhos</h4>
                    <ul>
                        <li><a href="#">Campanhas</a></li>
                        <li><a href="#">Casa</a></li>
                        <li><a href="#">Feminino</a></li>
                        <li><a href="#">Kids</a></li>
                        <li><a href="#">Masculino</a></li>
                        <li><a href="#">Promoções</a></li>
                    </ul>
                </li>            
                <li>
                    <h4>Sac Loja Logo <span>0800-701-0316</span></h4>
                    <button class="bar-code">Solicitar Boleto</button>
                </li>
            </ul>
            <small>2015. Lojas Logo. Todos os direitos reservados</small>
        </div>
        <div id="featured half">
            <img src="<?php bloginfo('template_directory'); ?>/assets/images/rodape.jpg" alt="">
			<small class="mkt">mkt virtual</small>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>