function showPosition(position) {
  alert(position.coords.latitude + position.coords.longitude);
}

function errorHandler(err) {
    if(err.code == 1) {
       alert("Error: Access is denied!");
    }

    else if( err.code == 2) {
       alert("Error: Position is unavailable!");
    }
 }

$('#cep-button').click(function(){
  var iframe = $('iframe[name=map]');
  var input = iframe.contents().find('#pac-input');
  input.val($('#cep-text').val());
  iframe[0].contentWindow.fireChanges();
});

var map;
function initMap() {
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: -34.397, lng: 150.644},
    zoom: 8
  });
}
