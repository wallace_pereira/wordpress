<header>
	<div class="top-bar">
        <div class="center">
            <form action="">
                <input type="text" placeholder="Buscar">
                <input type="submit" value="">
            </form>
            <ul>
                <li><a href="#">Peça seu cartão de cliente</a></li>
                <li><a href="#"><img src="<?php bloginfo('template_directory'); ?>/assets/images/cod-barras-branco.png" alt="">Solicite a 2º via de boleto</a></li>
                <li><a href="#">Encontre uma loja</a></li>
                <li><a href="#">Assine a newsletter</a></li>
            </ul>
        </div>
    </div>
    <div class="menu-center">
        <div id="banners">
            <div>
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/banner1.jpg" />
            </div>
            <div>
                <img src="<?php bloginfo('template_directory'); ?>/assets/images/banner2.jpg" />
            </div>
        </div>
        <div class="info">
            <h1>
                <a href="#">
                    <img src="<?php bloginfo('template_directory'); ?>/assets/images/logo.png" alt="Logo">
                </a>
            </h1>
            <nav>
                <ul>
                    <li>
                        <a href="#">Campanhas</a>
                    </li>
                    <li>
                        <a href="">Feminino</a>
                    </li>
                    <li>
                        <a href="">Masculino</a>
                    </li>
                    <li>
                        <a href="">Kids</a>
                    </li>
                    <li>
                        <a href="">Casa</a>
                    </li>
                    <li>
                        <a href="">Promoções</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</header>
<?php wp_head(); ?>